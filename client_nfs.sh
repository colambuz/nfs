#!/bin/bash
yum install -y nfs-utils
systemctl enable nfs-server
systemctl start nfs-server
mkdir /mnt/nfs_share
mkdir /mnt/nfs_share/upload
mount -t nfs -o vers=3 10.0.0.41:/var/nfs_share/ /mnt/nfs_share/
mount -t nfs -o vers=3 10.0.0.41:/var/nfs_share/upload/ /mnt/nfs_share/upload/
echo "10.0.0.41:/var/nfs/ /mnt/nfs_share/ nfs defaults  0 0" >> /etc/fstab
echo "10.0.0.41:/var/upload/ /mnt/nfs_share/upload/ nfs defaults 0 0" >> /etc/fstab

