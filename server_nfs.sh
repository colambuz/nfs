#!/bin/bash
yum install -y nfs-utils firewalld

systemctl enable nfs-server
systemctl start nfs-server

mkdir /var/nfs_share
mkdir /var/nfs_share/upload
chmod o+rw /var/nfs_share
chmod o+rw /var/nfs_share/upload

echo "/var/nfs_share 10.0.0.40(rw)" >> /etc/exports
exportfs -rav
systemctl restart nfs-server

systemctl enable firewalld
systemctl start firewalld
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --permanent --zone=public --add-service=mountd
firewall-cmd --permanent --zone=public --add-service=rpc-bind
firewall-cmd --permanent --zone=public --add-port=4001/udp
firewall-cmd --permanent --zone=public --add-port=4001/tcp
firewall-cmd --permanent --zone=public --add-port=2049/udp
firewall-cmd --permanent --zone=public --add-port=2049/tcp
firewall-cmd --reload
systemctl restart firewalld

selinuxenabled && setenforce 0

cat >/etc/selinux/config<<__EOF
SELINUX=disabled
SELINUXTYPE=targeted
__EOF
